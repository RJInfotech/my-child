﻿using System;
using Child.Circle.External;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Child.Circle.Test
{
    [TestClass]
    public class ChildProblemSolverTest
    {
        private IChildProblemHelper helper;
        private ChildProblemSolver subject;
        [TestInitialize]
        public void Initialize()
        {
            helper = Substitute.For<IChildProblemHelper>();
            subject = new ChildProblemSolver(helper);
        }

        [TestMethod]
        public void Solve_ZeroChildren()
        {
            helper.GetGameInput().Returns(new GameInput()
            {
                ChildrenCount = 0,
                EliminateEach = 5,
                Id = 1
            });

            subject.Solve();

            helper.DidNotReceive().PostGameOutput(Arg.Any<GameOutput>());
        }

        [TestMethod]
        public void Solve_NegativeChildren()
        {
            helper.GetGameInput().Returns(new GameInput()
            {
                ChildrenCount = -1,
                EliminateEach = 5,
                Id = 1
            });

            subject.Solve();

            helper.DidNotReceive().PostGameOutput(Arg.Any<GameOutput>());
        }

        [TestMethod]
        public void Solve_ZeroEliminate()
        {
            helper.GetGameInput().Returns(new GameInput()
            {
                ChildrenCount = 5,
                EliminateEach = 0,
                Id = 1
            });

            subject.Solve();

            helper.DidNotReceive().PostGameOutput(Arg.Any<GameOutput>());
        }

        [TestMethod]
        public void Solve_NegativeEliminate()
        {
            helper.GetGameInput().Returns(new GameInput()
            {
                ChildrenCount = 5,
                EliminateEach = -1,
                Id = 1
            });

            subject.Solve();

            helper.DidNotReceive().PostGameOutput(Arg.Any<GameOutput>());
        }

        [TestMethod]
        public void Solve_EliminateLessThanCount()
        {
            helper.GetGameInput().Returns(new GameInput()
            {
                ChildrenCount = 3,
                EliminateEach = 2,
                Id = 1
            });

            subject.Solve();

            helper.Received(1).PostGameOutput(Arg.Is<GameOutput>(o => o.LastChild == 3 && o.OrderOfElimination[0] == 2 && o.OrderOfElimination[1] == 1));
        }

        [TestMethod]
        public void Solve_EliminateEqualToCount()
        {
            helper.GetGameInput().Returns(new GameInput()
            {
                ChildrenCount = 3,
                EliminateEach = 3,
                Id = 1
            });

            subject.Solve();

            helper.Received(1).PostGameOutput(Arg.Is<GameOutput>(o => o.LastChild == 2 && o.OrderOfElimination[0] == 3 && o.OrderOfElimination[1] == 1));
        }

        [TestMethod]
        public void Solve_EliminateGreaterThanCount()
        {
            helper.GetGameInput().Returns(new GameInput()
            {
                ChildrenCount = 3,
                EliminateEach = 5,
                Id = 1
            });

            subject.Solve();

            helper.Received(1).PostGameOutput(Arg.Is<GameOutput>(o => o.LastChild == 1 && o.OrderOfElimination[0] == 2 && o.OrderOfElimination[1] == 3));
        }
    }
}
