# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Consider the following children’s game:  

* n children stand around a circle.   
* Starting with a given child and working clockwise, each child gets a sequential number, which we will refer to as its id.   
* Then starting with the first child, they count out from 1 until k. The k'th child is now out and leaves the circle. The count starts again with the child immediately next to the eliminated one.  
* Children are so removed from the circle one by one. The winner is the last child left standing.  
 

### Running complexity ###

* Initialize the array with values - O(n)
* Solving problem - O(n) - 1

Hence the running complexity is 2 * O(n)-1

### Difficulty of the problem ###

The main challenge of the problem is to find the right balance between time and space.  We need less time to find the winner and less space to identify the right order of the elimination.  

### Improvements can be made ###

* PACT test (as a consumer) to record all interactions
* Comments on the source code in both class / function

### Who do I talk to? ###

* Ramji Piramanayagam