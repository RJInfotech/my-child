﻿namespace Child.Circle
{
    public interface IChildProblemSolver
    {
        void Solve();
    }
}