﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Child.Circle.Configuration;
using Child.Circle.External;

namespace Child.Circle
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var container = AutofacHelper.GetContainer();

                IChildProblemSolver solver = container.Resolve<IChildProblemSolver>();
                    // new ChildProblemSolver(new ChildProblemHelper(new ChildProblemConfiguration()));
                solver.Solve();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Sorry! an error occurred. {ex.Message}.  Please try again later.");
            }
            Console.WriteLine("Enter any key to exit");
            Console.Read();
        }
    }
}
