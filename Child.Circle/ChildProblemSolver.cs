﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Child.Circle.External;
using Newtonsoft.Json;

namespace Child.Circle
{
    /// <summary>
    /// Solves problem for children standing in a circle
    /// </summary>
    public class ChildProblemSolver : IChildProblemSolver
    {
        private readonly IChildProblemHelper _helper;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="helper">Api Helper to get input and post output</param>
        public ChildProblemSolver(IChildProblemHelper helper)
        {
            _helper = helper;
        }

        /// <summary>
        /// Solver method to get input and post output
        /// </summary>
        public void Solve()
        {
            // Getting input
            var input = _helper.GetGameInput();

            //starting stop watch
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Console.WriteLine($"Input: {JsonConvert.SerializeObject(input)}");

            // Do not process invalid inputs
            if (input.ChildrenCount < 1 || input.EliminateEach < 1)
            {
                Console.WriteLine("Invalid elimination process");
                return;
            }

            // Initialize children with values (id)
            List<int> children = new List<int>();
            for (int i = 1; i <= input.ChildrenCount; i++)
            {
                children.Add(i);
            }
            List<int> eliminatedChildren = new List<int>();

            // Recursive mthod to find out the winner
            Solve(children, eliminatedChildren, input.EliminateEach, 0);
            var output = new GameOutput()
            {
                Id = input.Id,
                LastChild = children[0],
                OrderOfElimination = eliminatedChildren
            };
            Console.WriteLine($"Problem solved in {sw.ElapsedMilliseconds} milliseconds");
            Console.WriteLine($"Output: {JsonConvert.SerializeObject(output)}");

            // Post the result to output.
            _helper.PostGameOutput(output);
        }

        private static void Solve(List<int> children, List<int> eliminatedChildren, int eliminate, int startIndex)
        {
            if (children.Count == 1)
            {
                return;
            }

            if (startIndex + eliminate <= children.Count)
            {
                var eliminateIndex = startIndex + eliminate - 1;
                eliminatedChildren.Add(children[eliminateIndex]);
                children.RemoveAt(eliminateIndex);
                startIndex = eliminateIndex == children.Count ? 0 : eliminateIndex;
            }
            else
            {
                int restLength = children.Count - startIndex;
                int restEliminate = eliminate - restLength;
                int nextIndex = restEliminate - ((int)(restEliminate / children.Count) * children.Count);
                if (nextIndex == 0)
                {
                    nextIndex = children.Count;
                }
                eliminatedChildren.Add(children[nextIndex - 1]);
                children.RemoveAt(nextIndex - 1);
                startIndex = nextIndex - 1 == children.Count ? 0 : nextIndex - 1;
            }

            Solve(children, eliminatedChildren, eliminate, startIndex);
        }
    }
}
