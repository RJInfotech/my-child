using System.Reflection;
using Autofac;

namespace Child.Circle
{
    public static class AutofacHelper
    {
        public static IContainer GetContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).AsImplementedInterfaces();
            return builder.Build();
        }
    }
}