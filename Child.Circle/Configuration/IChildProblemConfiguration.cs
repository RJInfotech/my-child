﻿namespace Child.Circle.Configuration
{
    public interface IChildProblemConfiguration
    {
        string EndpointApi { get; }
    }
}