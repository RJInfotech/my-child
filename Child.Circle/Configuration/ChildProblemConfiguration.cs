﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Child.Circle.Configuration
{
    public class ChildProblemConfiguration : IChildProblemConfiguration
    {
        public string EndpointApi { get; }

        public ChildProblemConfiguration()
        {
            EndpointApi = ConfigurationManager.AppSettings["Api.Endpoint"];
        }
    }
}
