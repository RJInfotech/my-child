﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Child.Circle.Configuration;
using RestSharp;

namespace Child.Circle.External
{
    public class ChildProblemHelper : IChildProblemHelper
    {
        private readonly IRestClient _restClient;

        private static class ApiEndpoint
        {
            public const string GameInput = "main/game";
            public const string GameOutput = "main/game/{id}";
        }
        public ChildProblemHelper(IChildProblemConfiguration configuration)
        {
            _restClient = new RestClient(configuration.EndpointApi);
        }

        public GameInput GetGameInput()
        {
            var request = new RestRequest(ApiEndpoint.GameInput, Method.GET);
            IRestResponse<GameInput> response = _restClient.Execute<GameInput>(request);
            return response.Data;
        }

        public void PostGameOutput(GameOutput output)
        {
            var request = new RestRequest(ApiEndpoint.GameOutput, Method.POST);
            request.AddParameter("id", output.Id);
            request.AddBody(output);
            _restClient.Execute(request);
        }
    }
}
