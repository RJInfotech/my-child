﻿namespace Child.Circle.External
{
    public interface IChildProblemHelper
    {
        GameInput GetGameInput();
        void PostGameOutput(GameOutput output);
    }
}