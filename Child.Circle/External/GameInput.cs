using RestSharp.Deserializers;

namespace Child.Circle.External
{
    public class GameInput
    {
        [DeserializeAs(Name = "id")]
        public int Id { get; set; }
        [DeserializeAs(Name = "children_count")]
        public int ChildrenCount { get; set; }
        [DeserializeAs(Name = "eliminate_each")]
        public int EliminateEach { get; set; }
    }
}