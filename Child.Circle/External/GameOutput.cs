﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace Child.Circle.External
{
    public class GameOutput
    {
        [DeserializeAs(Name = "id")]
        public int Id { get; set; }
        [DeserializeAs(Name = "last_child")]
        public int LastChild { get; set; }
        [DeserializeAs(Name = "order_of_elimination")]
        public List<int> OrderOfElimination { get; set; }

    }
}